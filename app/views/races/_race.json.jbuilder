json.extract! race, :id, :year, :round, :circuit_id, :name, :date, :created_at, :updated_at
json.url race_url(race, format: :json)

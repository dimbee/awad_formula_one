json.extract! driver, :id, :forename, :surname, :dob, :nationality, :created_at, :updated_at
json.url driver_url(driver, format: :json)

json.extract! result, :id, :race_id, :driver_id, :constructor_id, :grid, :position, :rank, :created_at, :updated_at
json.url result_url(result, format: :json)
